package org.monEntreprise;

import java.util.ArrayList;
import java.util.List;

public class Avion {
    private String modele;
    private List<Siege> sieges;

    public Avion(String modele) {
        this.modele = modele;
        Siege siege1 = new Siege("A", 1);
        Siege siege2 = new Siege("B", 1);
        this.sieges = new ArrayList<>();
        this.sieges.add(siege1);
        this.sieges.add(siege2);
    }

    public String getModele() {
        return modele;
    }

    public List<Siege> getSieges() {
        return sieges;
    }
}
