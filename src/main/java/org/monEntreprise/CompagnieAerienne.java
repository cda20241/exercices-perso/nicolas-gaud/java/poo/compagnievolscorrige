package org.monEntreprise;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CompagnieAerienne implements AgenceDeVoyage{
    private String nom;
    private List<Vol> vols;
    private Integer numVol;

    public CompagnieAerienne(String nom) {
        this.nom = nom;
        this.numVol = 0;
        this.vols = new ArrayList<>();
    }

    public void creerVol(
            String villeDepart,
            String villeArrivee,
            Date dateDepart,
            Date dateArrivee) {
        this.numVol += 1;
        Vol vol = new Vol(numVol, dateDepart, dateArrivee, villeDepart, villeArrivee);
        this.vols.add(vol);
        vol.setCompagnie(this);
    }

    public void annulerVol(Vol vol_annule) {
        this.vols.remove(vol_annule);
        vol_annule.setCompagnie(null);
    }

    public void afficherVols() {
        for(Vol vol : this.vols) {
            System.out.println(vol);
        }
    }

    public List<Vol> getVolsByVilleDepart(String villeDepart) {
        List<Vol> listeVols = new ArrayList<>();
        for (Vol vol : this.vols) {
            if (Objects.equals(vol.getVilleDepart(), villeDepart)) {
                listeVols.add(vol);
            }
        }
        return listeVols;
    }

    @Override
    public Billet commanderBillet(Voyage voyageAReserver) {
        /**
         * Si la compagnie ne peut pas proposer de siège au client pour le vol demandé,
         * alors la fonction retourne null.
         * Si on peut proposer un billet pour le vol, alors on renvoie un billet, et on
         * place le client sur le premier siège disponible dans l'avion, on ne permet pas
         * au client de choisir lui-même son siège.
         */
        Vol volAReserver = null;
        Billet billet = new Billet(50);
        boolean volDeCompagnie = false;
        for (Vol vol : this.vols) {
            if (vol.getNumVol() == voyageAReserver.getCodeVoyage()) {
                volAReserver = vol;
                volDeCompagnie = true;
            }
        }
        if (volDeCompagnie == false) {
            return null;
        }
        // je me suis assuré que le vol à réserver est bien affreté
        // par cette compagnie

        Avion avionAffretePourLeVol = volAReserver.getAvion();
        List<Siege> sieges = avionAffretePourLeVol.getSieges();
        // je récupère la liste des sièges de l'avion associé à ce vol,
        // et je vais regarder s'il y a un siège disponible
        boolean siegeDisponible = false;
        for (Siege siege : sieges) {
            if (siege.getBillet() == null) {
                // dès que je trouve un siège disponible, je l'associe à mon billet
                // je ne laisse pas la possibilité au client de choisir son billet je
                // lui mets le premier siège disponible
                siegeDisponible = true;
                billet.setSiege(siege);
                siege.setBillet(billet);
            }
        }
        if (siegeDisponible == true) {
            return billet;
        }
        else {
            return null;
        }
    }
}
