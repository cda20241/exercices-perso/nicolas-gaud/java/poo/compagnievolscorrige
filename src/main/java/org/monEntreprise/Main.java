package org.monEntreprise;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Entrée with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        CompagnieAerienne compagnieAerienne1 = new CompagnieAerienne("A");
        CompagnieAerienne compagnieAerienne2 = new CompagnieAerienne("B");
        compagnieAerienne1.creerVol("Berlin", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18));
        compagnieAerienne1.creerVol("Paris", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18));
        compagnieAerienne1.afficherVols();
        List<Vol> volsParis = compagnieAerienne1.getVolsByVilleDepart("Paris");
        Vol v1 = volsParis.get(0);
        for (Vol vol : volsParis) {
            compagnieAerienne1.annulerVol(vol);
        }
        System.out.println("Vols pour Paris annulés");
        compagnieAerienne1.afficherVols();
        List<Integer> l1 = new ArrayList<>();
        List<Integer> l2 = new Vector<>();
        l1.isEmpty();
        OfficeDeTourisme office1 = new OfficeDeTourisme();
        office1.acheterVoyage(compagnieAerienne1, new Voyage(2));
    }
}