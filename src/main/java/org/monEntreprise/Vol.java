package org.monEntreprise;

import java.util.Date;

public class Vol {
    private Integer numVol;
    private Date dateDepart;
    private Date dateArrivee;
    private String villeDepart;
    private String villeArrivee;
    private CompagnieAerienne compagnieAerienne;
    private Avion avion;

    public Vol(Integer numVol, Date dateDepart, Date dateArrivee, String villeDepart, String villeArrivee) {
        this.numVol = numVol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        int a = 1;
        String b = String.valueOf(a);
    }

    @Override
    public String toString() {
        return this.numVol + " " + this.villeDepart + " " + this.villeArrivee + " " + this.dateDepart + " " + this.dateArrivee;
    }

    public Integer getNumVol() {
        return numVol;
    }

    public Date getDateDepart() {
        return dateDepart;
    }

    public Date getDateArrivee() {
        return dateArrivee;
    }

    public String getVilleDepart() {
        return villeDepart;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public CompagnieAerienne getCompagnie() {
        return compagnieAerienne;
    }

    public Avion getAvion() {
        return avion;
    }

    public void setNumVol(Integer numVol) {
        this.numVol = numVol;
    }

    public void setDateDepart(Date dateDepart) {
        this.dateDepart = dateDepart;
    }

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public void setCompagnie(CompagnieAerienne compagnieAerienne) {
        this.compagnieAerienne = compagnieAerienne;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }
}
