package org.monEntreprise;

public class Billet {
    private Integer prix;
    private Siege siege;

    public Billet(Integer prix) {
        this.prix = prix;
    }

    public Integer getPrix() {
        return prix;
    }

    public Siege getSiege() {
        return siege;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public void setSiege(Siege siege) {
        this.siege = siege;
    }
}
