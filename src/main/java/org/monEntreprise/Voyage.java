package org.monEntreprise;

public class Voyage {
    private Integer codeVoyage;

    public Voyage(Integer codeVoyage) {
        this.codeVoyage = codeVoyage;
    }

    public Integer getCodeVoyage() {
        return codeVoyage;
    }
}
