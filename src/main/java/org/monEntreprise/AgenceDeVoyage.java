package org.monEntreprise;

public interface AgenceDeVoyage {
    public Billet commanderBillet(Voyage voyage);
}
