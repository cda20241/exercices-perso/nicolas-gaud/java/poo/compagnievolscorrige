package org.monEntreprise;

public class OfficeDeTourisme {
    public OfficeDeTourisme() {
    }

    public void acheterVoyage(AgenceDeVoyage agence, Voyage voyage){
        agence.commanderBillet(voyage);
    }
}
